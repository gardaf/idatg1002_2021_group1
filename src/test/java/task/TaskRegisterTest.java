package task;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TaskRegisterTest {
    TaskRegister register;
    Task testTask;

    @BeforeEach
    void createRegister() {
        this.register = new TaskRegister("target/testRegister.json");
        testTask = new Task("Test", "Test", "Test", "12/03/2002");
        this.register.addTask(testTask);
    }

    @AfterEach
    void removeRegister() {

        this.register.getTaskRegister().clear();
        this.register.exportTasksAsJson("target/testRegister.json");
    }

    @Test
    @DisplayName("Test add task to register")
    void addTask() {
        this.register.addTask(new Task("Test1", "Test1", "Test", "12/03/2002"));
        assertEquals(2, this.register.getTaskRegister().size());
    }

    @Test
    @DisplayName("Test edit name of task in register")
    void editName() {
        String newName = "NewName";
        this.register.editName(testTask, newName);
        assertEquals(newName, testTask.getTaskName());

    }

    @Test
    @DisplayName("Test edit priority of task in the register")
    void editPriority() {
        String newPriority = "High";
        this.register.editPriority(testTask, newPriority);
        assertEquals(newPriority, testTask.getTaskPriority());
    }

    @Test
    @DisplayName("Test edit due date of task in the register")
    void editDueDate() {
        String newDueDate = "2021/03/21";
        this.register.editDueDate(testTask, newDueDate);
        assertEquals(newDueDate, testTask.getTaskDueDate());
    }

    @Test
    @DisplayName("test edit task state of task in the register")
    void editTaskState() {
        this.register.editTaskState(testTask, true);
        assertTrue(testTask.isTaskStatus());
    }

    @Test
    @DisplayName("Test removing task from register")
    void removeTask() {
        this.register.removeTask(testTask);
        assertEquals(0, this.register.getTaskRegister().size());
    }
}