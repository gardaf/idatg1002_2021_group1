module idatg1002_2021_group.gui.main {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires com.fasterxml.jackson.databind;
    requires json.simple;
    requires log4j;
    opens task;
    opens gui;
    exports task;
    exports gui;
}