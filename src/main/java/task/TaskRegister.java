package task;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * This class is used to add tasks in a register and handle persistence storage with json.
 */
public class TaskRegister {
    private ObservableList<Task> taskRegister;
    private File jsonFile;
    private ObjectMapper objectMapper;
    private static final Logger LOGGER = Logger.getLogger(TaskRegister.class.getName());

    /**
     * Instantiates a new Task register containing a register with data from json-file or creates a empty register.
     */
    public TaskRegister() {
        this.taskRegister = FXCollections.observableArrayList();
        this.jsonFile = new File("target/taskRegister.json");
        this.objectMapper = new ObjectMapper();
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        JavaType listType = objectMapper
                .getTypeFactory()
                .constructCollectionType(ArrayList.class, Task.class);
        if (this.jsonFile.exists()) {
            try {
                ArrayList<Task> importedTasks = objectMapper.readValue(this.jsonFile, listType);
                this.taskRegister = FXCollections.observableArrayList(importedTasks);
                LOGGER.info("Imported " + this.taskRegister.size() + " tasks from " + this.jsonFile.getAbsolutePath());
            } catch (IOException e) {
                LOGGER.error("An error occurred while trying to import tasks from " + jsonFile.getAbsolutePath(), e);
            }
        } else {
            taskRegister = FXCollections.observableArrayList();
            LOGGER.info("No preexisting json file was found, a new one will be created at " + jsonFile.getAbsolutePath());
        }
    }


    /**
     * Instantiates a new Task register with a given path for the json file.
     *
     * @param jsonPath path for json file
     */
    public TaskRegister(String jsonPath) {
        this.taskRegister = FXCollections.observableArrayList();
        this.jsonFile = new File(jsonPath);
        this.objectMapper = new ObjectMapper();
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        JavaType listType = objectMapper
                .getTypeFactory()
                .constructCollectionType(ArrayList.class, Task.class);
        if (this.jsonFile.exists()) {
            try {
                ArrayList<Task> importedTasks = objectMapper.readValue(this.jsonFile, listType);
                this.taskRegister = FXCollections.observableArrayList(importedTasks);
                LOGGER.info("Imported " + this.taskRegister.size() + " tasks from " + this.jsonFile.getAbsolutePath());
            } catch (IOException e) {
                LOGGER.error("An error occurred while trying to import tasks from " + jsonFile.getAbsolutePath(), e);
            }
        } else {
            taskRegister = FXCollections.observableArrayList();
            LOGGER.info("No preexisting json file was found, a new one will be created at " + jsonFile.getAbsolutePath());
        }
    }

    /**
     * Add task to register and writes to json.
     *
     * @param task Task to be added.
     */
    public void addTask(Task task) {
        this.taskRegister.add(task);
        writeToJson();
        LOGGER.info("The task \"" + task.getTaskName() + "\" was added to " + jsonFile.getName());
    }

    /**
     * Edit name of a given task and writes to json.
     *
     * @param task        the task to be changed
     * @param taskNameNew the task name new
     */
    public void editName(Task task, String taskNameNew) {
        this.taskRegister.get(taskRegister.indexOf(task)).setTaskName(taskNameNew);
        writeToJson();
    }

    /**
     * Edit priority of a given task and writes to json.
     *
     * @param task         the task to be changed
     * @param taskPriority the task priority
     */
    public void editPriority(Task task, String taskPriority) {
        this.taskRegister.get(taskRegister.indexOf(task)).setTaskPriority(taskPriority);
        writeToJson();
    }

    /**
     * Edit due date of a given task and writes to json.
     *
     * @param task        the task to be changed
     * @param taskDueDate the task due date
     */
    public void editDueDate(Task task, String taskDueDate) {
        this.taskRegister.get(taskRegister.indexOf(task)).setTaskDueDate(taskDueDate);
        writeToJson();
    }

    /**
     * Edit task state (if task is ongoing or completed) of a given task and writes to json.
     *
     * @param task       the task to be changed
     * @param taskStatus the task status
     */
    public void editTaskState(Task task, boolean taskStatus) {
        this.taskRegister.get(taskRegister.indexOf(task)).setTaskStatus(taskStatus);
        writeToJson();
    }

    /**
     * Edit description of a given task and writes to json.
     *
     * @param task            the task to be changed
     * @param taskDescription the task description
     */
    public void editTaskDescription(Task task, String taskDescription) {
        this.taskRegister.get(taskRegister.indexOf(task)).setTaskDescription(taskDescription);
        writeToJson();
    }

    /**
     * Remove task a given task from register, this change is reflected in json.
     *
     * @param task the task to be removed
     */
    public void removeTask(Task task) {
        this.taskRegister.remove(task);
        writeToJson();
        LOGGER.info("The task \"" + task.getTaskName() + "\" was removed from " + jsonFile.getName());
    }


    /**
     * Exports the current task register sa a json file to a given path.
     *
     * @param path The path where the json will be saved.
     */
    public void exportTasksAsJson(String path) {
        try {
            objectMapper.writeValue(new File(path), this.taskRegister);
            LOGGER.info(taskRegister.size() + "tasks were exported to " + path);
        } catch (IOException e) {
            LOGGER.error("An error occurred wile trying to write taskRegister to " + path, e);
        }
    }

    /**
     * Imports tasks from a given JSON file and filters out the tasks that have the wrong status.
     *
     * @param path           The path of the import file.
     * @param statusFilter Whether tasks should be filtered by task status.
     * @param registerStatus The task status to not filter away.
     */
    public void importTasksFromJson(String path, boolean statusFilter, boolean registerStatus) {
        File importFile = new File(path);
        JavaType listType = objectMapper
                .getTypeFactory()
                .constructCollectionType(ArrayList.class, Task.class);
        int importSize = 0;
        if (importFile.exists()) {
            try {
                ArrayList<Task> importedTasks = objectMapper.readValue(importFile, listType);

                if (statusFilter) {
                    List<Task> filteredImportList = importedTasks.stream().filter(task -> task.isTaskStatus() == registerStatus).collect(Collectors.toList());
                    this.taskRegister.addAll(filteredImportList);
                    importSize = filteredImportList.size();
                } else {
                    this.taskRegister.addAll(importedTasks);
                    importSize = importedTasks.size();
                }

                if (importSize > 0) {
                    writeToJson();

                    LOGGER.info(importSize + " tasks were imported from " + importFile.getAbsolutePath());
                }
            } catch (IOException e) {
                LOGGER.error("An error occurred while trying to import taskRegister from " + importFile.getAbsolutePath(), e);
            }
        } else {
            LOGGER.error("The chosen file does not exist: " + importFile.getAbsolutePath());
        }
    }

    /**
     * Get task register observablelist.
     *
     * @return the observablelist
     */
    public ObservableList<Task> getTaskRegister() {
        return this.taskRegister;
    }

    /**
     * Writes the current task register to a json file.
     */
    private void writeToJson() {
        try {
            this.objectMapper.writeValue(this.jsonFile, this.taskRegister.toArray());
        } catch (IOException e) {
            LOGGER.error("An error occurred while trying to write taskRegister to " + jsonFile.getAbsolutePath(), e);
        }
    }
}
