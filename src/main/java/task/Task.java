package task;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;

import java.time.LocalDate;

/**
 * This class is used to create a task object which holds data about a single task.
 */
public class Task {
    private SimpleStringProperty taskName;
    private SimpleStringProperty taskDescription;
    private SimpleStringProperty taskPriority;
    private LocalDate taskStartDate;
    private SimpleStringProperty taskDueDate;
    private boolean taskStatus;
    private CheckBox checkBox;

    /**
     * Instantiates a new empty task.
     */
    public Task() {
        this.taskName = new SimpleStringProperty();
        this.taskDescription = new SimpleStringProperty();
        this.taskPriority = new SimpleStringProperty();
        this.taskDueDate = new SimpleStringProperty();
    }


    /**
     * Instantiates a new Task.
     *
     * @param taskName        the task name
     * @param taskDescription the task description
     * @param taskPriority    the task priority
     * @param taskDueDate     the task due date
     */
    public Task(String taskName, String taskDescription, String taskPriority, String taskDueDate) {
        this.taskName = new SimpleStringProperty(taskName);
        this.taskDescription = new SimpleStringProperty(taskDescription);
        this.taskPriority = new SimpleStringProperty(taskPriority);
        this.taskStartDate = LocalDate.now();
        this.taskDueDate = new SimpleStringProperty(taskDueDate);
        this.taskStatus = false;
    }

    /**
     * Gets task name.
     *
     * @return the task name
     */
    public String getTaskName() {
        return taskName.get();
    }

    /**
     * Gets the task name as stringproperty.
     *
     * @return task name as stringproperty
     */
    public final StringProperty taskNameProperty() {
        return taskName;
    }

    /**
     * Gets the task priority as stringproperty.
     *
     * @return task name as stringproperty
     */
    public final StringProperty taskPriorityProperty() {
        return taskPriority;
    }

    /**
     * Gets the task duedate as stringproperty.
     *
     * @return task duedate as stringproperty
     */
    public final StringProperty taskDouDateProperty() {
        return taskDueDate;
    }

    /**
     * Sets task name.
     *
     * @param taskName the task name
     */
    public void setTaskName(String taskName) {
        this.taskName = new SimpleStringProperty(taskName);
    }

    /**
     * Gets task priority.
     *
     * @return the task priority
     */
    public String getTaskPriority() {
        return taskPriority.get();
    }

    /**
     * Sets task priority.
     *
     * @param taskPriority the task priority
     */
    public void setTaskPriority(String taskPriority) {
        this.taskPriority = new SimpleStringProperty(taskPriority);
    }

    /**
     * Gets task start date.
     *
     * @return the task start date
     */
    @JsonIgnore
    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    /**
     * Gets the task's start date as a string.
     *
     * @return The task's start date as a string.
     */
    @JsonGetter("taskStartDate")
    public String getTaskStartDateAsString() {
        return this.taskStartDate.toString();
    }

    /**
     * Sets the task's start date from a string.
     *
     * @param taskStartDate The String start date to be set.
     */
    @JsonSetter("taskStartDate")
    public void setTaskStartDateFromString(String taskStartDate) {
        this.taskStartDate = LocalDate.parse(taskStartDate);
    }

    /**
     * Gets the checkbox object stored in the task.
     *
     * @return The tasks checkbox object.
     */
    @JsonIgnore
    public CheckBox getCheckBox() {
        return checkBox;
    }

    /**
     * Sets the checkbox object to be stored in the task.
     *
     * @param checkBox The checkbox to be set.
     */
    @JsonIgnore
    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }

    /**
     * Centers the checkbox in it's column.
     *
     * @return The centered checkbox.
     */
    public CheckBox centerCheckBox() {
        CheckBox checkBox = new CheckBox();
        checkBox.setCenterShape(true);
        checkBox.setAlignment(Pos.CENTER);
        return checkBox;
    }

    /**
     * Sets task start date.
     *
     * @param taskStartDate the task start date
     */
    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    /**
     * Gets task due date.
     *
     * @return the task due date
     */
    public String getTaskDueDate() {
        return taskDueDate.get();
    }

    /**
     * Sets task due date.
     *
     * @param taskDueDate the task due date
     */
    public void setTaskDueDate(String taskDueDate) {
        this.taskDueDate = new SimpleStringProperty(taskDueDate);
    }

    /**
     * Gets the task description
     *
     * @return Task description
     */
    @JsonGetter("taskDescription")
    public String getTaskDescription() {
        return taskDescription.get();
    }

    /**
     * Gets the task description as SimpleStringProperty
     *
     * @return Task description
     */
    public SimpleStringProperty taskDescriptionProperty() {
        return taskDescription;
    }

    /**
     * Sets the task description
     *
     * @param taskDescription Task description as string
     */
    @JsonSetter("taskDescription")
    public void setTaskDescription(String taskDescription) {
        this.taskDescription.set(taskDescription);
    }

    /**
     * Is task status boolean.
     *
     * @return the boolean
     */
    public boolean isTaskStatus() {
        return taskStatus;
    }

    /**
     * Sets task status.
     *
     * @param taskStatus the task status
     */
    public void setTaskStatus(boolean taskStatus) {
        this.taskStatus = taskStatus;
    }
	
	/**
	 * Gets a list containing the priorities.
	 *
	 * @return a list containing the priorities
	 */
	public static List<String> getPriorityOptions() {
        return List.of("None", "Low", "Medium", "High");
    }
}
