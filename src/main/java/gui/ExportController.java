package gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import task.Task;
import task.TaskRegister;


import java.io.File;
import java.io.IOException;

/**
 * The type Export controller.
 */
public class ExportController {
    private static final Logger LOGGER = Logger.getLogger(ExportController.class.getName());
    File file;
    private String url;
    @FXML
    public TextField path;
    @FXML
    public Button exportButton;
    private Stage thisStage;
    private final Controller controller;
    private TaskRegister taskRegisterToDo;
    private TaskRegister taskRegisterDone;


    /**
     * Instantiates a new Export controller.
     *
     * @param controller the controller
     */
    public ExportController(Controller controller) {
        this.controller = controller;
        thisStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Export.fxml"));
        loader.setController(this);
        try {
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Export");
            thisStage.getIcons().add(new Image("/images/to-do.png"));
        } catch (IOException | RuntimeException e) {
            LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
        }
    }


    /**
     * Show stage.
     *
     * @param taskRegisterDone1 the task register done 1
     * @param taskRegisterToDo1 the task register to do 1
     */
    public void showStage(TaskRegister taskRegisterDone1, TaskRegister taskRegisterToDo1) {
        taskRegisterToDo = taskRegisterToDo1;
        taskRegisterDone = taskRegisterDone1;
        thisStage.showAndWait();
    }

    /**
     * Start scene.
     *
     * @param task the task
     */
    public void startScene(ObservableList<Task> task) {
        thisStage.showAndWait();
    }

    /**
     * Find folder.
     */
    public void findFolder() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Find Folder");
        File selectedDirectory = chooser.showDialog(thisStage);
        path.setText(selectedDirectory.getPath());
        url = selectedDirectory.getPath();
    }

    /**
     * Export json.
     */
    public void exportJson() {
        String status = "no tasks to export";
        if (!taskRegisterToDo.getTaskRegister().isEmpty()) {
            taskRegisterToDo.exportTasksAsJson(url + "/tasks_done.json");
            status = "exported to : " + url + "/tasks_done.json";
        }
        if (!taskRegisterDone.getTaskRegister().isEmpty()) {
            taskRegisterDone.exportTasksAsJson(url + "/tasks_todo.json");
            status = "exported to : " + url + "/tasks_todo.json";
        }
        if (!taskRegisterToDo.getTaskRegister().isEmpty() && !taskRegisterDone.getTaskRegister().isEmpty()) {
            status = "tasks_done.json and tasks_todo.json were exported to : " + url;
        }
        controller.setStatusMessage(status);
        exit();
    }


    /**
     * Exit.
     */
    public void exit() {
        Stage stage = (Stage) exportButton.getScene().getWindow();
        stage.close();
    }
}
