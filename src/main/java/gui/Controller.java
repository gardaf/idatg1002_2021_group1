package gui;

import java.util.Comparator;
import javafx.animation.PauseTransition;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.util.Duration;
import task.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import task.TaskRegister;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * This class is used to control the main view.
 */
public class Controller implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());
    @FXML
    public TableColumn<Task, String> c1;
    @FXML
    public TableColumn<Task, String> c2;
    @FXML
    public TableColumn<Task, String> c3;
    @FXML
    public TableColumn<Task, String> c4;
    @FXML
    public TableView<Task> tableOngoing;
    @FXML
    public TableColumn<Task, String> c1DoneTask;
    @FXML
    public TableColumn<Task, String> c2DoneTask;
    @FXML
    public TableColumn<Task, String> c3DoneTask;
    @FXML
    public TableColumn<Task, String> c4DoneTask;
    @FXML
    public TableView<Task> tableDone;
    @FXML
    public Button addTaskBtn;
	@FXML
	public TextField statusMessageOutput;
    @FXML
    public TabPane tabPane;
	
	double newFontSize;
    private final Stage thisStage;
    private TaskRegister taskRegisterToDo;
    private TaskRegister taskRegisterDone;
    private Task selectedTaskOngoing;
    private Task selectedTaskDone;
	
	/**
	 * Instantiates a new Controller. This constructor load the main view.
	 */
    public Controller() {
        thisStage = new Stage();
        thisStage.setMinWidth(360);
        thisStage.setMinHeight(300);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Main.fxml"));
        loader.setController(this);
        try {
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Task Organizer");
            thisStage.getIcons().add(new Image("/images/to-do.png"));
        } catch(IOException | RuntimeException e) {
            LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
        }
        setMenuOnTask();
    }
	
	/**
	 * Initialize method run each time the view is loaded.
	 * Sets up the table and fills if with tasks.
	 */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LOGGER.info("Starting gui application!");
        taskRegisterToDo = new TaskRegister("target/tasks_todo.json");
        taskRegisterDone = new TaskRegister("target/tasks_done.json");

        c1.setCellValueFactory(new PropertyValueFactory<>("taskName"));
        c2.setCellValueFactory(new PropertyValueFactory<>("taskPriority"));
        c3.setCellValueFactory(new PropertyValueFactory<>("taskDouDate"));
        c4.setCellValueFactory(new PropertyValueFactory<Task, String>("checkBox"));
        
        addCheckBoxToOngoing();
        tableOngoing.setItems(taskRegisterToDo.getTaskRegister());

        c1DoneTask.setCellValueFactory(new PropertyValueFactory<>("taskName"));
        c2DoneTask.setCellValueFactory(new PropertyValueFactory<>("taskPriority"));
        c3DoneTask.setCellValueFactory(new PropertyValueFactory<>("taskDouDate"));
        c4DoneTask.setCellValueFactory(new PropertyValueFactory<Task, String>("checkBox"));
        
        c2.setComparator(Comparator.comparingInt(p -> Task.getPriorityOptions().indexOf(p)));
        c2DoneTask.setComparator(Comparator.comparingInt(p -> Task.getPriorityOptions().indexOf(p)));

        addCheckBoxToDone();
        tableDone.setItems(taskRegisterDone.getTaskRegister());
    }
	
	/**
	 * Deleting a selected task from the table.
	 */
	public void removeSelected(){
        if (tabPane.getSelectionModel().isSelected(0)){
            try{
                alertDelete(selectedTaskOngoing, 0);
            } catch (NullPointerException npe){
                npe.printStackTrace();
            }
        }else {
            try{
                alertDelete(selectedTaskDone, 1);
            } catch (NullPointerException npe){
                npe.printStackTrace();
            }
        }
    }
	
	/**
	 * This method sets and handle checkboxes for each task in completed tasks tab.
	 */
    public void addCheckBoxToDone(){
        for (Task task : taskRegisterDone.getTaskRegister()) {
            task.setCheckBox(task.centerCheckBox());
            task.getCheckBox().setSelected(true);
            task.getCheckBox().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    task.setTaskStatus(false);
                    addTaskBackToOngoingTask(task);
                }
            });
        }
    }
	
	/**
	 * This method sets and handle checkboxes for each task in ongoing tasks tab.
	 */
	public void addCheckBoxToOngoing(){
        for (Task task : taskRegisterToDo.getTaskRegister()) {
            task.setCheckBox(task.centerCheckBox());
            task.getCheckBox().setOnAction(new EventHandler<>() {
                @Override
                public void handle(ActionEvent e) {
                    task.setTaskStatus(true);
                    removeTask(task);
                }
            });
        }
    }

	/**
	 * This method will show a menu when right-clicking a task with the possibility to choose edit or delete,
	 * or open a detail-view of a task if double-clicked.
	 */
    public void setMenuOnTask() {
        if (tableOngoing != null) {
            tableOngoing.setRowFactory(tableView -> {
                final TableRow<Task> row = new TableRow<>();
                final ContextMenu rowMenu = new ContextMenu();

                MenuItem removeItem = new MenuItem("Delete");
                removeItem.setOnAction(e -> {
                    alertDelete(row.getItem(), 0);
                });
                MenuItem editItem = new MenuItem("Edit");
                editItem.setOnAction(e -> {
                    EditTaskController editTaskController = new EditTaskController(this, row.getItem(), taskRegisterToDo);
                    editTaskController.showStage();
                });
                rowMenu.getItems().addAll(removeItem, editItem);
                row.contextMenuProperty().bind(
                        Bindings.when(Bindings.isNotNull(row.itemProperty()))
                                .then(rowMenu)
                                .otherwise((ContextMenu) null));
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        ViewTaskController viewTaskController = new ViewTaskController(this, row.getItem());
                        viewTaskController.showStage();
                    }
                    selectedTaskOngoing = row.getItem();
                });
                return row;
            });
        }
        if(tableDone != null) {
            tableDone.setRowFactory(tableView -> {
                final TableRow<Task> row = new TableRow<>();
                final ContextMenu rowMenu = new ContextMenu();

                MenuItem removeItem = new MenuItem("Delete");
                removeItem.setOnAction(e -> {
                    alertDelete(row.getItem(), 1);
                });
                rowMenu.getItems().addAll(removeItem);
                row.contextMenuProperty().bind(
                        Bindings.when(Bindings.isNotNull(row.itemProperty()))
                                .then(rowMenu)
                                .otherwise((ContextMenu) null));
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        ViewTaskController viewTaskController = new ViewTaskController(this, row.getItem());
                        viewTaskController.showStage();
                    }
                    selectedTaskDone = row.getItem();
                });
                return row;
            });
        }
    }
	
	/**
	 * This method changes the state of a given task.
	 *
	 * @param name new name
	 * @param date new deadline
	 * @param priority new priority
	 * @param description new description
	 * @param task task to be changed
	 */
    public void updateTask(String name, String date, String priority, String description, Task task) {
        taskRegisterToDo.editName(task, name);
        taskRegisterToDo.editDueDate(task, date);
        taskRegisterToDo.editPriority(task, priority);
        taskRegisterToDo.editTaskDescription(task, description);
        taskRegisterToDo.getTaskRegister().size();
        setStatusMessage("Task changed successfully");
    }
	
	/**
	 * This method loads the add task view.
	 */
    @FXML
    public void openAddTaskWindow(javafx.event.ActionEvent actionEvent) {
        AddTaskController addTaskController = new AddTaskController(this);
        addTaskController.showStage();
    }

    public void showStage() {
        thisStage.showAndWait();
    }
	
	/**
	 * This method adds a task to the register.
	 *
	 * @param task task to be added
	 */
    @FXML
    public void addTaskToList(Task task) {
        task.setCheckBox(task.centerCheckBox());
        task.getCheckBox().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                task.setTaskStatus(true);
                removeTask(task);
            }
        });
        taskRegisterToDo.addTask(task);
		setStatusMessage("Task added successfully");
    }
	
	/**
	 * This method move a task from ongoing table to completed table.
	 *
	 * @param task task to be moved
	 */
    public void removeTask(Task task) {
        if (task.isTaskStatus() == true) {
            task.setCheckBox(task.centerCheckBox());
            task.getCheckBox().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    task.setTaskStatus(false);
                    addTaskBackToOngoingTask(task);
                }
            });
            task.getCheckBox().setSelected(true);
            taskRegisterDone.addTask(task);
            taskRegisterToDo.removeTask(task);
            setStatusMessage("Task moved to completed tasks");
        }
    }
	
	/**
	 * This method move a task from completed table to ongoing table.
	 *
	 * @param task task to be moved
	 */
    public void addTaskBackToOngoingTask(Task task) {
        if (task.isTaskStatus() == false) {
            task.setCheckBox(task.centerCheckBox());
            task.getCheckBox().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    task.setTaskStatus(true);
                    removeTask(task);
                }
            });
            addTaskToList(task);
            taskRegisterDone.removeTask(task);
            setStatusMessage("Task moved to ongoing tasks");
        }
    }
	
	/**
	 * This method show an alert and delete task if accepted.
	 *
	 * @param task task to be removed
	 * @param listNr which table the task is located
	 */
    public void alertDelete(Task task, int listNr) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remove Task");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete task " + task.getTaskName());

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            if (listNr == 0) {
                taskRegisterToDo.removeTask(task);
            } else {
                taskRegisterDone.removeTask(task);
            }
			setStatusMessage("Task removed successfully");
        } else {
            alert.close();
        }
    }
	
	/**
	 * This method scales font based on window size.
	 */
    public void fontScaling() {
        thisStage.widthProperty().addListener((obs, oldVal, newVal) -> {
            if (newVal.intValue() < 600) {
                newFontSize = 0.9;
                addTaskBtn.styleProperty().bind(Bindings.format("-fx-font-size:1.5em;"));
                statusMessageOutput.styleProperty().bind(Bindings.format("-fx-font-size:1em; -fx-background-color:transparent; -fx-font-weight:bold;"));

            } else if (newVal.intValue() < 1000) {
                newFontSize = 1.2;
                addTaskBtn.styleProperty().bind(Bindings.format("-fx-font-size:2em;"));
                statusMessageOutput.styleProperty().bind(Bindings.format("-fx-font-size:1.5em; -fx-background-color:transparent; -fx-font-weight:bold;"));

            } else if (newVal.intValue() < 1400) {
                newFontSize = 1.5;
                addTaskBtn.styleProperty().bind(Bindings.format("-fx-font-size:2.5em;"));
                statusMessageOutput.styleProperty().bind(Bindings.format("-fx-font-size:2em; -fx-background-color:transparent; -fx-font-weight:bold;"));

            }else {
                newFontSize = 2;
                addTaskBtn.styleProperty().bind(Bindings.format("-fx-font-size:3em;"));
                statusMessageOutput.styleProperty().bind(Bindings.format("-fx-font-size:2.5em; -fx-background-color:transparent; -fx-font-weight:bold;"));

            }
            tableOngoing.styleProperty().bind(Bindings.format("-fx-font-size: " + newFontSize + "em;"));
            tableDone.styleProperty().bind(Bindings.format("-fx-font-size: " + newFontSize + "em;"));

        });
    }

	
	/**
	 * Method to load about window.
	 */
    public void showAboutWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Task Organizer");
        alert.setContentText("Task Organizer application made by IDATG1002 - Group 1\n"
                + "Special thanks to @SimenBai\n"
                + "version 1.0.0\n"
                + "30.04.2021");
        alert.showAndWait();
    }
    public void openImport(){
        ImportController importController = new ImportController(this);
        importController.showStage(taskRegisterToDo, taskRegisterDone);
    }
    
    public void openExport(){
        ExportController exportController = new ExportController(this);
        exportController.showStage(taskRegisterToDo, taskRegisterDone);

    }
	
	/**
	 * Method to load user-manual.
	 */
    public void showHelpWindow() {
        File pdfFile = new File("src/main/resources/pdf/User-manual.pdf");
        try{
            Desktop.getDesktop().open(pdfFile);
            setStatusMessage("User manual opened in browser!");
        } catch (IOException e) {
            LOGGER.error("An error occurred while trying to open " + pdfFile.getAbsolutePath(), e);
        }
    }
	
	/**
	 * Print a message to status message textfield for 5 seconds, then clearing itself.
	 *
	 * @param statusMessage message to be printed in message field
	 */
    public void setStatusMessage(String statusMessage) {
    	statusMessageOutput.setText(statusMessage);
		PauseTransition wait = new PauseTransition(Duration.seconds(5));
		wait.setOnFinished((e) -> {
			statusMessageOutput.clear();
		});
		wait.play();
	}
}

