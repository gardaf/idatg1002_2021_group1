package gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import task.Task;


/**
 * This class is used to control the view for viewing a task.
 */
public class ViewTaskController implements Initializable {
	private static final Logger LOGGER = Logger.getLogger(ViewTaskController.class.getName());
	@FXML
	public TextField taskNameOutput;
	@FXML
	public TextField taskPriorityOutput;
	@FXML
	public TextField taskStartDateOutput;
	@FXML
	public TextField taskDueDateOutput;
	@FXML
	public Button okButton;
	@FXML
	public TextArea taskDescriptionOutput;
	
	public Task task;
	private final Controller controller;
	private Stage thisStage;
	
	/**
	 * Instantiates a new ViewTask Controller. This constructor load the task details view.
	 *
	 * @param controller the controller
	 * @param task task to be viewed
	 */
	public ViewTaskController(Controller controller, Task task) {
		this.controller = controller;
		this.task = task;
		thisStage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ViewTask.fxml"));
		loader.setController(this);
		try {
			thisStage.setScene(new Scene(loader.load()));
			thisStage.setTitle("Task Details");
			thisStage.getIcons().add(new Image("/images/to-do.png"));
		} catch(IOException | RuntimeException e) {
			LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
		}
	}
	
	/**
	 * Show this stage
	 */
	public void showStage() {
		thisStage.showAndWait();
	}
	
	/**
	 * This method sets the data from chosen task to text fields in the view.
	 */
	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		taskNameOutput.setText(task.getTaskName());
		taskDescriptionOutput.setText(task.getTaskDescription());
		taskPriorityOutput.setText(task.getTaskPriority());
		taskStartDateOutput.setText(task.getTaskStartDate().toString());
		taskDueDateOutput.setText(task.getTaskDueDate());
	}
	
	/**
	 * Method closing the view when ok button is pressed.
	 */
	@FXML
	public void okButtonAction(ActionEvent event) {
		Stage stage = (Stage) okButton.getScene().getWindow();
		stage.close();
	}
	
	public Task getTask() {
		return task;
	}
}
