package gui;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import task.Task;
import javafx.scene.control.DatePicker;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * This class is used to control the view for adding a task.
 */
public class AddTaskController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(AddTaskController.class.getName());
    @FXML
    public TextField taskNameField;
    @FXML
    public ChoiceBox taskPriorityChoiceBox;
    @FXML
    public DatePicker taskDatePicker;
    @FXML
    public Button cancelButton;
    @FXML
    public Button ok;
    @FXML
	public TextField taskDescriptionField;
	
	private Task task;
    private Stage thisStage;
    private final Controller controller;
	
	/**
	 * Instantiates a new AddTask Controller. This constructor loads the AddTask view.
	 *
	 * @param controller the controller
	 */
	public AddTaskController(Controller controller){
        this.controller = controller;
        // Create the new stage
        thisStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AddTask.fxml"));
        loader.setController(this);
        try {
            thisStage.setScene(new Scene(loader.load()));
            // Setup the window/stage
            thisStage.setTitle("Add Task");
            thisStage.getIcons().add(new Image("/images/to-do.png"));
        } catch(IOException | RuntimeException e) {
            LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
        }
    }
	
	/**
	 * Shows this stage.
	 */
	public void showStage() {
        thisStage.showAndWait();
    }
	
	/**
	 * Initialize method run each time the view is loaded.
	 * This code changes the datepicker such that a user is not allowed to select a date before current date.
	 */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    	taskPriorityChoiceBox.setItems(FXCollections.observableList(Task.getPriorityOptions()));
    	taskPriorityChoiceBox.getSelectionModel().selectFirst();
    	
    	// This code changes the datepicker such that a user is not allowed to select a date before current date
		taskDatePicker.setDayCellFactory(picker -> new DateCell() {
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate currentDate = LocalDate.now();
				setDisable(empty || date.compareTo(currentDate) < 0);
			}
		});
    }
	
	/**
	 * Gets text from input fields and adds a new task to register.
	 * If error throw, show alert with information to the user.
	 *
	 * @throws NullPointerException throws nullpointerexception if date is null
	 * @throws IOException throws ioexception if task name is empty
	 */
    public void addTask() throws NullPointerException, IOException {
		try {
			if(taskNameField.getText().equals("")) {
				throw new IOException("Empty string");
			}
			task = new Task(taskNameField.getText(),taskDescriptionField.getText(), taskPriorityChoiceBox.getValue().toString(),taskDatePicker.getValue().toString());
			controller.addTaskToList(task);
			Stage stage = (Stage) ok.getScene().getWindow();
			stage.close();
		} catch(IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid input");
			alert.setHeaderText("Task name cannot be empty!");
			alert.setContentText("Please enter task name");
			alert.showAndWait();
		} catch(NullPointerException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid input");
			alert.setHeaderText("Deadline cannot be empty!");
			alert.setContentText("Please select a deadline");
			alert.showAndWait();
		}
    }

    public void startScene(ObservableList<Task> taskToDo){
        thisStage.showAndWait();
    }
	
	/**
	 * Method to handle cancel button.
	 */
    @FXML
    public void cancelButtonAction(javafx.event.ActionEvent actionEvent) throws IOException{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit alert");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to close?");
        
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK){
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        } else {
            alert.close();
        }
    }
    
	public Task getTask() {
        return task;
    }
}
