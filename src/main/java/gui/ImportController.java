package gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import task.Task;
import task.TaskRegister;

import java.io.File;
import java.io.IOException;

/**
 * This class is used to control the view for importing a tasklist
 */
public class ImportController {
    private static final Logger LOGGER = Logger.getLogger(ImportController.class.getName());
    File file;

    @FXML
    public TextField path;
    @FXML
    public Button importButton;
    private Stage thisStage;
    private final Controller controller;
    private TaskRegister taskRegisterDone;
    private TaskRegister taskRegisterToDo;

    /**
     * Instantiates a new Import controller.
     *
     * @param controller the controller
     */
    public ImportController(Controller controller) {
        this.controller = controller;
        thisStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Import.fxml"));
        loader.setController(this);
        try {
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Import");
            thisStage.getIcons().add(new Image("/images/to-do.png"));
        } catch (IOException | RuntimeException e) {
            LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
        }
    }

    /**
     * Show stage.
     *
     * @param taskRegisterToDo1 the task register to do 1
     * @param taskRegisterDone1 the task register done 1
     */
    public void showStage(TaskRegister taskRegisterToDo1, TaskRegister taskRegisterDone1) {
        taskRegisterToDo = taskRegisterToDo1;
        taskRegisterDone = taskRegisterDone1;
        thisStage.showAndWait();
    }

    /**
     * Start scene.
     * @param task the task
     */
    public void startScene(ObservableList<Task> task) {
        thisStage.showAndWait();
    }

    /**
     * Find file, with the .json format
     */
    public void findFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("json", "*.json"));
        file = fileChooser.showOpenDialog(thisStage.getScene().getWindow());
        path.setText(file.getAbsolutePath());
    }

    /**
     * Import json.
     */
    public void importJSON() {
        taskRegisterDone.importTasksFromJson(path.getText(), true, true);
        controller.addCheckBoxToDone();

        taskRegisterToDo.importTasksFromJson(path.getText(), true, false);
        controller.addCheckBoxToOngoing();

        controller.setStatusMessage("imported from : " + path.getText());
        exit();
    }

    /**
     * Exit.
     */
    public void exit() {
        Stage stage = (Stage) importButton.getScene().getWindow();
        stage.close();
    }
}
