package gui;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import task.Task;
import task.TaskRegister;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * This class is used to control the view for editing a task.
 */
public class EditTaskController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(TaskRegister.class.getName());
    @FXML
    public TextField taskNameField;
    @FXML
    public ChoiceBox taskPriorityChoiceBox;
    @FXML
    public DatePicker taskDatePicker;
    @FXML
    public Button cancelButton;
    @FXML
    public Button ok;
    @FXML
    public TextField taskDescriptionField;
    
    private TaskRegister taskRegister;
	private Task task;
    private Stage thisStage;
    private final Controller controller;
	
	/**
	 * Instantiates a new EditTask Controller. This constructor load the Edit task view.
	 *
	 * @param controller the controller
	 * @param task task to be edited
	 * @param taskRegister which table the task is located
	 */
    public EditTaskController(Controller controller, Task task, TaskRegister taskRegister){
        this.controller = controller;
        this.task = task;
        this.taskRegister = taskRegister;
        thisStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EditTask.fxml"));
        loader.setController(this);
        try {
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Edit Task");
            thisStage.getIcons().add(new Image("/images/to-do.png"));
        } catch(IOException | RuntimeException e) {
            LOGGER.error("An error occurred trying to load " + loader.getClass().getName(), e);
        }
    }
	
	/**
	 * Shows this stage.
	 */
	public void showStage() {
        thisStage.showAndWait();
    }
	
	/**
	 * Initialize method run each time the view is loaded.
	 * Sets the text fields with data from chosen task and set up datepicker such that a user is not allowed to select a date before current date.
	 */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        taskNameField.setText(task.getTaskName());
        taskDatePicker.setAccessibleText(task.getTaskDueDate());
        taskPriorityChoiceBox.setItems(FXCollections.observableList(Task.getPriorityOptions()));
        taskPriorityChoiceBox.getSelectionModel().selectFirst();
        taskPriorityChoiceBox.setValue(task.getTaskPriority());
        taskDescriptionField.setText(task.getTaskDescription());
		taskDatePicker.setDayCellFactory(picker -> new DateCell() {
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate currentDate = LocalDate.now();
				setDisable(empty || date.compareTo(currentDate) < 0);
			}
		});
    }
	
	/**
	 * Gets text from input fields and edit the task with given input.
	 * If error throw, show alert with information to the user.
	 *
	 * @throws NullPointerException throws nullpointerexception if date is null
	 * @throws IOException throws ioexception if task name is empty
	 */
    public void updateTask() throws NullPointerException, IOException {
		try {
			if(taskNameField.getText().equals("")) {
				throw new IOException("Empty string");
			}
			controller.updateTask(taskNameField.getText(), taskDatePicker.getValue().toString(), taskPriorityChoiceBox.getValue().toString(), taskDescriptionField.getText(), task);
            controller.tableOngoing.getColumns().get(0).setVisible(false);
            controller.tableOngoing.getColumns().get(0).setVisible(true);
			Stage stage = (Stage) ok.getScene().getWindow();
			stage.close();
		} catch(NullPointerException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid input");
			alert.setHeaderText("Deadline cannot be empty!");
			alert.setContentText("Please select a deadline");
			alert.showAndWait();
		} catch(IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid input");
			alert.setHeaderText("Task name cannot be empty!");
			alert.setContentText("Please enter task name");
			alert.showAndWait();
		}
    }

    public void startScene(ObservableList<Task> taskToDo){
        thisStage.showAndWait();
    }
	
	/**
	 * Method handling cancel button.
	 */
    @FXML
    public void cancelButtonAction(javafx.event.ActionEvent actionEvent) throws IOException{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit alert");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to close?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK){
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        } else {
            alert.close();
        }
    }

    public Task getTask() {
        return task;
    }
}
