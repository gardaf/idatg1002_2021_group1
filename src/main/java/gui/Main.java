package gui;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Class used to launch application
 */
public class Main extends Application {
    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler());
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Controller controller1 = new Controller();
        controller1.showStage();
    }
}