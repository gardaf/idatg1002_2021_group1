Javadoc can be read here -> [Javadoc](http://gardaf.pages.stud.idi.ntnu.no/idatg1002_2021_group1/)
## **Installation manual**

## Installing and running the application
General information. The user will be given a zip file, containing all files needed to run the application. 

1. To run the application, Java is needed. Go to [Java](https://java.com/en/download/) and follow their guide on how to download, and install. 
2. After step 1, navigate to  [Releases](https://gitlab.stud.idi.ntnu.no/gardaf/idatg1002_2021_group1/-/releases/v0.2.0) and download the latest release from the development team. 
3. From here you will need to extract the zip-file to you chosen location. 
4. After extraction is completed, you can now run the .jar-file to run the application. 


## Building the application from source
To get the latest version of the to-do application it needs to be built from source.

1. Download and install java 14 by following the instructions [here](https://www.oracle.com/java/technologies/javase/jdk14-archive-downloads.html). Remember to add java to PATH, you can get instructions for this [here](https://www.qualitestgroup.com/resources/knowledge-center/how-to-guide/add-java-system-path/). If you are on a Linux system you can install it with your preferred package manager. On Debian systems this can be done by putting the following in the terminal:
   ```$ sudo apt install openjdk-14-headless```. Installing with a package manager should add java to PATH automatically, if not follow the instructions [here](https://www.qualitestgroup.com/resources/knowledge-center/how-to-guide/add-java-system-path/)
2. Install maven from [here](https://maven.apache.org/install.html) and add it to PATH as shown on the maven website. On Linux systems you can install it with your package manager. Install command for Debian systems: ```$ sudo apt get maven```.
3. Open the terminal and change directory to where you want the app installed. This can be done with the cd command:
   ```cd <path to directory>```
4. Clone the GitLab repository with this command: ```git clone https://gitlab.stud.idi.ntnu.no/gardaf/idatg1002_2021_group1.git```
5. Cd into the installed directory: ```cd idatg1002_2021_group1```
6. Run the command to clean the maven project: ```mvn clean```
7. Compile the application with javafx: ```mvn javafx:compile```
8. To run the application insert: ```mvn javafx:run```

<br/><br/><br/><br/>
# **User manual** _Can also be read under [Wiki](https://gitlab.stud.idi.ntnu.no/gardaf/idatg1002_2021_group1/-/wikis/System/Installation-and-User-manual) with guided pictures_
# Basic Introduction
Once the application is opened, it will represent an empty list, with two given tabs. The user can browse these tabs to navigate between "Ongoing task" and "Completed task". There will also be a menu-bar with different buttons that do different things. These are explained below with the few different functionalities. Start by first reading the important notes!

- [Important notes](#important-notes)
- [Scaling text](#scaling-text)
- [Adding a task](#adding-a-task)
- [Editing a task](#editing-a-task)
- [Delete a task](#deleting-a-task)
- [Sorting/reordering tasks](#sorting/reordering-tasks)
- [Marking tasks as complete](#marking-tasks-as)
- [Marking tasks as NOT complete](#marking-tasks-as-not-complete)
- [Viewing task description](#viewing-task-description)
- [Importing task list](#importing-task-list)
- [Saving/Exporting task list](#saving/Exporting-task-list)


#

## Important notes

- For all actions made within the application will provide a status bar on the bottom of the window describing what has been done.

- When adding or editing a task, the user will need to make the correct inputs. All data
needs input before adding/modifying the task (Except “Description”).
- Name, and description is added in whatever way the user wants, however, “Name”
cannot be empty!
- When putting in the deadline, click the little calendar icon besides the text-input. This will prompt a classic calendar window. From here, select the date when you would like the tasks deadline to be. Remember, user cannot input dates prior to current date.



#

## Scaling text

For scaling text within the application

- The user can scale the main window, by hovering one of the lower corners to scale the window size, while holding and dragging. The text will scale with it!
The picture below is an example by hovering the lower right corner.

 

#

## Adding a task

For adding a task the user will start by

- Clicking the big plus icon in the lower right-hand corner, reading "Add Task". When clicking the button, a window will pop up.
- This is where the user will be filling in data/information about their new to be added task. There are textfields where the application needs user input.
- After the data has been filled in, the user can then choose whether to abort the process or add the task to their list by clicking "OK".

#

## Editing a task

For editing a task the user will need to

- Right click on the table&#39;s row corresponding with the task that the user wants changing. Then left click on "Edit". 
- This will open a similar window to the "Add task" window.In this window, apply the changes, just like adding a task in that current window.
- Same as the adding task, the user can now choose to either cancel the process, or to save changes made.

#

## Deleting a task

In order to delete a task, the user can

- Do this in the "Completed tasks"-tab or "Ongoing task"-tab.
- Simply mark a task in the tables row, and right click the row to open the menu for that task.
- Here, pressing the delete-option will prompt a confirmation window in case the user was to highlight the wrong task.
- Now, the user will now have to confirm the deletion or cancel.


#

## Sorting/reordering tasks

For sorting the tasks, the user can either Do this by name, priority or due date. To do this

- Simply click on the columns title that corresponds with one of those tags above. **NB! The columns titles can be clicked multiple times in order to sort in ascending-, descending order or default(no sorting/newly added tasks).**
- As for reordering, the user could edit the task, and then sort it resulting in reordering the task in the list.

#

## Marking tasks as complete

To mark a task as complete

- Navigate to the "On-going"-tab.
- In this tab, go to the far-right column that has the completion status; "Progress".
- Simply mark the task, by clicking the check-box to send that task to the "Completed task"-tab.


#

## Marking tasks as NOT complete

To mark a task as NOT complete

- Navigate to the "Completed task"-tab.
- In this tab, go to the far-right column that has the completion status; "Progress"
- Simply un-mark the task the user would like to mark as not complete, by clicking the checkbox to send that task to the "Ongoing task"-tab.

#

## Viewing task description

In order to view a task description

- The user would have to double-click on the task to prompt the "Task description" window.
- This window will represent all the data of the task

#

## Importing task list

To import an already existing task list

- Click on "File" from the menu selector (top left corner), then import.
- This window will allow the user to open file manager/browser and navigate to the .JSON format file is located. Click the file, and then "Import".
- The list will then be imported to the current list.


#

## Saving/Exporting task list

To save a task list you can do this in two ways from the application

- Click on "File" from the menu selector (top left corner), then "Export". Browse to the location you want the saved list. From here, click "Export", and the list will be saved in JSON format locally.
- When adding a task to the application, it will automatically save to two separate files that is created locally, under root of the project/target/


